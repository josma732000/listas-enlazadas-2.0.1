/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package practica;

/**
 *
 * @author Jose
 */
class Lista {
     protected Nodo inicio, fin; 
    public Lista(){
        inicio = null;
        fin    = null;
    }
    
    public void agregarAlInicio(String elemento){
        inicio = new Nodo(elemento, inicio);
        if (fin == null){
            fin = inicio;
        }
    }
    public void mostrarListaEnlazada(){
        Nodo recorrer = inicio;
        System.out.println("");
        while (recorrer != null){
            System.out.print("["+ recorrer.dato+"] -->");
            recorrer = recorrer.siguiente;
        }
        System.out.println("");
    }
    
    public String borrarDelInicio(){
        String elemento = inicio.dato;
        if (inicio == fin){
            inicio = null;
            fin    = null;
        } else {
            inicio = inicio.siguiente;
        }
        return elemento;
    }
    
    public boolean esVacia(){
        if (inicio == null){
            return true;
        }else{
            return false;
        }
    }
    
    public void AgregarAlFinal(String elemento){
        if (!esVacia()){
            fin.siguiente = new Nodo(elemento);
            fin = fin.siguiente;
        }else{
            inicio=fin= new Nodo(elemento);
        }
    }
    
    public String borrarDelFinal(){
        String elemento = fin.dato;
        if(inicio ==fin){
            inicio=fin=null;
        } else{
            Nodo temporal = inicio;
            // Encontrar el final de la Lista
            while(temporal.siguiente !=fin){
                temporal = temporal.siguiente;
            }
            fin = temporal;
            fin.siguiente = null;
        }
        return elemento;
    }
}

