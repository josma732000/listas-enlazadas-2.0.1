/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package practica;

/**
 *
 * @author Jose
 */
class Nodo {
    public String dato;    
    public Nodo siguiente; 
    
   
    public Nodo(String d){
        this.dato = d;
      
        this.siguiente = null;
    }
 
    public Nodo(String d, Nodo n){
        dato = d;
        siguiente = n;
    }
}
